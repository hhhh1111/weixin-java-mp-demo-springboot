package com.github.binarywang.demo.wechat.builder;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMusicMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutVoiceMessage;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
public class VoiceBuilder {

  public WxMpXmlOutMessage build(String mediaId, WxMpXmlMessage wxMessage,
                                 WxMpService service) {

    WxMpXmlOutVoiceMessage m = WxMpXmlOutMessage.VOICE()
            .fromUser(wxMessage.getToUser())
            .toUser(wxMessage.getFromUser())
            .mediaId(mediaId)
            .build();

    return m;
  }
}
