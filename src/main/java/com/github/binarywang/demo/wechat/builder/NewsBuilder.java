package com.github.binarywang.demo.wechat.builder;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutNewsMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutVoiceMessage;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
public class NewsBuilder {

  public WxMpXmlOutMessage build(WxMpXmlMessage wxMessage,
                                 WxMpService service, WxMpXmlOutNewsMessage.Item... items) {

    WxMpXmlOutNewsMessage m = WxMpXmlOutMessage.NEWS()
            .fromUser(wxMessage.getToUser())
            .toUser(wxMessage.getFromUser())
            .addArticle(items)
            .build();

    return m;
  }
}
