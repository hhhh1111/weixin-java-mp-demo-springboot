package com.github.binarywang.demo.wechat.builder;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutImageMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMusicMessage;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
public class MusicBuilder {

  public WxMpXmlOutMessage build(String title, String description, String hqMusicUrl, String musicUrl, String thumbMediaId, WxMpXmlMessage wxMessage,
                                 WxMpService service) {

    WxMpXmlOutMusicMessage m = WxMpXmlOutMessage.MUSIC()
            .fromUser(wxMessage.getToUser())
            .toUser(wxMessage.getFromUser())
            .title(title)
            .description(description)
            .hqMusicUrl(hqMusicUrl)
            .musicUrl(musicUrl)
            .thumbMediaId(thumbMediaId)
            .build();

    return m;
  }
}
