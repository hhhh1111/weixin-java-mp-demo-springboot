package com.github.binarywang.demo.wechat.builder;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutVideoMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutVoiceMessage;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
public class VideoBuilder {

  public WxMpXmlOutMessage build(String title, String desc, String mediaId, WxMpXmlMessage wxMessage,
                                 WxMpService service) {

    WxMpXmlOutVideoMessage m = WxMpXmlOutMessage.VIDEO()
            .fromUser(wxMessage.getToUser())
            .toUser(wxMessage.getFromUser())
            .title(title)
            .description(desc)
            .mediaId(mediaId)
            .build();

    return m;
  }
}
